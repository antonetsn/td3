int max2(int a, int b){
    if (a > b){
        return a;
    }
    else{
        return b;
    }
}

int max3(int a, int b, int c){
    int maxAB = max2(a,b);
    int maxABC = max2(maxAB, c);
    return maxABC;
}

void ex1(){
    //ex1 max2
    Ut.afficherSL("saisir a");
    int a = Ut.saisirEntier();
    Ut.afficherSL("saisir b");
    int b = Ut.saisirEntier();

    Ut.afficherSL("max2 est : "+max2(a, b));
    //ex1 max3
    Ut.afficherSL("saisir c");
    int c = Ut.saisirEntier();
    Ut.afficher("max3 est : "+max3(a, b, c));
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres(int n) {



    int nbCh = 0;

    while(n > 0){
        n = n/10;
        nbCh++;
    }
    if(nbCh == 0){
        nbCh = 1;
    }



    return nbCh;
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){
    int nbCh = 0;
    n = n*n;
    while(n > 0){
        n = n/10;
        nbCh++;
    }
    if(nbCh == 0){
        nbCh = 1;
    }
    return nbCh;
}

/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car) {
    for (int j = 1; j <= nb; j++) {
        Ut.afficher(car);

    }
}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple(int h  ,char  c ){





    //A FAIRE




}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */

void afficheNombresCroissants(int nb1,int nb2 ){
    if(nb1<=nb2){
        for (int i=nb1; i<=nb2; i++)
        {
            Ut.afficher(i%10);
        }
    }
}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */

void afficheNombresDecroissants (int nb1,int nb2 ){
    if(nb1<=nb2){
        for (int i=nb2; i>=nb1; i--)
        {
            Ut.afficher(i%10);
        }
    }
}

void pyramideElaboree(int h ) {
    int espace = h;

    for(int i = 1 ; i <= h; i++) {
        espace--;

        afficheNombresCroissants(i , 2*i-1);
        afficheNombresDecroissants(i , 2*i-2);
        Ut.afficherSL("");
    }
}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c ){

    if(Math.sqrt(c)%1 == 0){
        int n = c/2;
        return n;
    }
    else{
        return -1;
    }

}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){

    if (racineParfaite(nb) == -1) {
        return false;
    }
    else{
        return true;
    }
}

/**
 *
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q){
    int sommeQ = 0;
    int sommeP = 0;

    for(int i = 1 ; i <= p-1 ; i++) {
        if (p % i == 0) {
            sommeP = sommeP + i;
        }
    }

    for(int i = 1 ; i <= q-1 ; i++){
        if(q%i == 0){
            sommeQ = sommeQ + i;
        }
    }

    if(sommeP == q && sommeQ == p){
        return true;
    }
    else{
        return false;
    }
}

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){
    for(int i = 0 ; i < max+1 ; i++){

        for(int j = i+1 ; j < max+1 ; j++){

            if (nombresAmicaux(i, j)==true){
                Ut.afficherSL(i+" et "+j+" sont amis.");
            }
        }
    }
}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){

    if (estCarreParfait(c1) || estCarreParfait(c2)) {
        return true;
    }
    else {
        return false;

    }
}


/**
 *
 * @param perimetre
 * @return une fonction qui calcule le nombre de triangles rectangles dont
 * les côtés sont des nombres entiers et dont le périmètre est inférieur à
 * un nombre ` n` passé en paramètre.
 */
/**
int  nombreTriangleRectangle (int perimetre) {

}
**/


void main (){
    afficheNombresCroissants(88,91);
    Ut.afficherSL("");
    afficheNombresDecroissants(88,91);
    Ut.afficherSL("");

    Ut.afficherSL(nbChiffres(0));
    Ut.afficherSL(nbChiffresDuCarre(0));

    Ut.afficherSL(nombresAmicaux(17296, 18416));

    afficheAmicaux(1210);
    Ut.afficherSL(racineParfaite(4));
    Ut.afficherSL(estCarreParfait(4)+" carre");
    Ut.afficherSL(estTriangleRectangle(40601,4059)+" traingle rec parfait");
    //Ut.afficherSL(nombreTriangleRectangle(6));
}

